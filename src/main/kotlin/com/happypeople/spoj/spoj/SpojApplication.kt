package com.happypeople.spoj.spoj

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpojApplication

fun main(args: Array<String>) {
	runApplication<SpojApplication>(*args)
}

