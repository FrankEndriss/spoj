package com.happypeople.spoj.first

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        PALIN().run()
    } catch (e: Throwable) {
        PALIN.log("" + e)
    }
}

class PALIN {
    fun run() {
        val sc = Scanner(systemIn())
        val n=sc.nextInt()
        (1..n).forEach {
            val k=sc.next()
            println(""+String(findPalin(k)))
        }
    }

    /** Find by subsequent increment beginning at center.
     **/
    fun findPalin(k: String):CharArray {
        if(k=="9")
            return "11".toCharArray()

        log("findPalin: ${k}")
        var kA=k.toCharArray()
        val L=(kA.size-1)/2
        val R=kA.size/2

        for(i in 0..L) // copy left half to right, mirrored
            kA[R+i]=kA[L-i]

        log("first attempt: ${String(kA)}")

        if(isBigger(kA, k.toCharArray()))
            return kA
        else
            return incrementPalindrome(kA, L, R)
    }

    private fun isBigger(kA: CharArray, k: CharArray): Boolean {
        for(i in kA.indices) {
            if (kA[i] > k[i])
                return true
            else if (kA[i] < k[i])
                return false
        }
        return false
    }

    private fun incrementPalindrome(kA: CharArray, pL: Int, pR: Int):CharArray {
        var L=pL
        var R=pR
        if(L<0) { // special case if kA is like "9999" construct "10001"
            var ret="1".toCharArray()
            kA.forEach { ret=ret.plus('0') }
            ret[ret.size-1]='1'
            return ret
        }

        // need to do this without recursion since input is huge
        while(L>=0) {
            if (kA[L] == '9') {
                kA[L--] = '0'
                kA[R++] = '0'
                // return incrementPalindrome(kA, L - 1, R + 1)
            } else {
                kA[L]++
                if (L != R)
                    kA[R]++
                return kA
            }
        }
        return incrementPalindrome(kA, L - 1, R + 1)
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}