package com.happypeople.spoj.first

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        ONP().run()
    } catch (e: Throwable) {
        ONP.log("" + e)
    }
}

/* see https://www.spoj.com/problems/ONP/
Simple recursion.
Convert:
(a+b) to
ab+ where
a and b is of form
a | (a+b)

So, 3 constructs:
Expr: start with "("
Var: "a-z"
Op: "+*-/^"

fun walkExpr(str, idx) {
    if(simple)
      output(simple)
    else
      idx=walkExpr(str, idx)
      op=next
      idx=walkExpr(str, idx)
    print op
}

*/
class ONP {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        for (i in (1..t)) {
            val ans=toRPN(sc.next())
            println("$ans")
        }
    }

    private fun toRPN(src: String): String {
        val out= mutableListOf<Char>()
        walkExpr(src, 0, out)
        return String(out.toCharArray())
    }

    private fun walkExpr(src: String, idx:Int, out:MutableList<Char>):Int {
        var lIdx=idx
        log("walkExpr, idx=$idx, out=$out")

        if(src[lIdx] == '(') {
            lIdx=walkExpr(src, lIdx+1, out)
        } else{
            out.add(src[lIdx])
            return lIdx+1
        }

        val op=src[lIdx++]
        lIdx=walkExpr(src, lIdx, out)
        out.add(op)
        return lIdx+1   // +1 to skip ")"
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}