package com.happypeople.spoj.first

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        First().run()
    } catch (e: Throwable) {
        First.log("" + e)
    }
}

class First {
    fun run() {
        val sc = Scanner(systemIn())
        while(true) {
            val i=sc.nextInt()
            if(i==42)
                return
            println("$i")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}