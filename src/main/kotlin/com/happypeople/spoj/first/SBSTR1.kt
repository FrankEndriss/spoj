package com.happypeople.spoj.first

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        SBSTR1().run()
    } catch (e: Throwable) {
        SBSTR1.log("" + e)
    }
}

class SBSTR1 {
    fun run() {
        val sc = Scanner(systemIn())
        while(sc.hasNext()) {
            val s1=sc.next()!!
            val s2=sc.next()!!
            val ans=if(s1.contains(s2)) 1 else 0
            println("$ans")
            log("$s1 $s2 $ans")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}