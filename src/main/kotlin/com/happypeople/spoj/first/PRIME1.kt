package com.happypeople.spoj.first

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.math.BigInteger
import java.util.*

fun main(args: Array<String>) {
    try {
        PRIME1().run()
    } catch (e: Throwable) {
        PRIME1.log("" + e)
    }
}

class PRIME1 {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        (1..t).forEach {
            val m=sc.nextInt()
            val n=sc.nextInt()
            var bi= BigInteger.valueOf(m.toLong())
            if(bi.isProbablePrime(16))
                println("$bi")
            while(true) {
                bi = bi.nextProbablePrime()
                if(bi.toInt()>n) {
                    println()
                    break
                }
                println("$bi")
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}