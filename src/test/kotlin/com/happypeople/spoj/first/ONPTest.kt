package com.happypeople.spoj.first

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class ONPTest {

    @Before
    fun setUp() {
        ONP.printLog=true
    }

    @Test
    fun run() {
        ONP.inputStr="3\n" +
                "(a+(b*c))\n" +
                "((a+b)*(z+x))\n" +
                "((a+t)*((b+(a+c))^(c+d)))\n"
        ONP().run()
    }
}