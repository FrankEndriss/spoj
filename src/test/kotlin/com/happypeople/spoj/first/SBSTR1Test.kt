package com.happypeople.spoj.first

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class SBSTR1Test {

    @Before
    fun setUp() {
        SBSTR1.printLog=true
    }

    @Test
    fun run() {
        SBSTR1.inputStr="1010110010 10110\n" +
                "1110111011 10011"
        SBSTR1().run()
    }
}