package com.happypeople.spoj.first

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import java.math.BigDecimal
import java.util.*

class PALINTest {

    @Before
    fun setUp() {
        PALIN.printLog=false
    }

    val testStr="12\n" +
    "808\n" +
    "2133\n"+
    "9999\n"+
    "999\n"+
    "1991\n"+
    "10001\n"+
    "100001\n"+
    "9\n"+
    "10\n"+
    "11\n"+
    "0\n"+
    "12312312312318888888823123123123\n"
    @Test
    fun run() {
        PALIN.inputStr=testStr
        PALIN().run()
    }

    @Test
    fun testAgainstSimpleFinder() {
        val p=PALIN()
        (0..10000).forEach {
            val s1=simpleFinder("$it")
            val s2=String(p.findPalin("$it"))
            assertEquals("input: $it", s1, s2)
        }
        (9999999995L..10000000009).forEach {
            val s1=simpleFinder("$it")
            val s2=String(p.findPalin("$it"))
            assertEquals("input: $it", s1, s2)
        }
        (9999999999995L..10000000000002).forEach {
            val s1=simpleFinder("$it")
            val s2=String(p.findPalin("$it"))
            assertEquals("input: $it", s1, s2)
        }
    }

    fun simpleFinder(str:String):String {
        var bi=BigDecimal(str)
        do {
            bi=bi.add(BigDecimal.ONE)
        }while(!isPalindrome(bi.toString()))
        return bi.toString()
    }

    fun isPalindrome(str:String):Boolean {
        var L=0
        var R=str.length-1
        while(L<R) {
            if(str[L++]!=str[R--])
                return false
        }
        return true
    }
}