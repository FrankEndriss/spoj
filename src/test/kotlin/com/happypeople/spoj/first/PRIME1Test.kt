package com.happypeople.spoj.first

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class PRIME1Test {

    @Before
    fun setUp() {
        PRIME1.printLog=true
    }

    @Test
    fun run() {
        PRIME1.inputStr="2\n"+
        "1 10\n"+
        "3 5\n"
        PRIME1().run()
    }
}